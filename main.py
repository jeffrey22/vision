import time
import json
import os
import shutil
from google.cloud import vision
from google.cloud.vision import types
from PIL import Image, ImageDraw
from google.cloud import storage
from werkzeug.utils import secure_filename
from flask import Flask, request, render_template
from flask_cors import CORS, cross_origin


def detect_face(face_file, max_results=4):
    """Uses the Vision API to detect faces in the given file.

    Args:
        face_file: A file-like object containing an image with faces.

    Returns:
        An array of Face objects with information about the picture.
    """
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "VisionProject_key.json"

    client = vision.ImageAnnotatorClient()

    content = face_file.read()
    image = types.Image(content=content)

    return client.face_detection(
        image=image, max_results=max_results).face_annotations


def highlight_faces(image, faces, output_filename):
    """Draws a polygon around the faces, then saves to output_filename.

    Args:
      image: a file containing the image with the faces.
      faces: a list of faces found in the file. This should be in the format
          returned by the Vision API.
      output_filename: the name of the image file to be created, where the
          faces have polygons drawn around them.
    """
    im = Image.open(image)
    draw = ImageDraw.Draw(im)
    # Sepecify the font-family and the font-size
    for face in faces:
        box = [(vertex.x, vertex.y)
               for vertex in face.bounding_poly.vertices]
        draw.line(box + [box[0]], width=5, fill='#00ff00')
        # Place the confidence value/score of the detected faces above the
        # detection box in the output image
        draw.text(((face.bounding_poly.vertices)[0].x,
                   (face.bounding_poly.vertices)[0].y - 30),
                  str(format(face.detection_confidence, '.3f')) + '%',
                  fill='#FF0000')
    im.save(output_filename)


def implicit():
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "VisionProject_key.json"
    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    storage_client = storage.Client()

    # Make an authenticated API request
    buckets = list(storage_client.list_buckets())
    print(buckets)


def get_image():
    # Image
    f = request.files['inpFile']
    test = f.filename.split('.')
    now = "{}".format(time.time()).replace('.', '-')
    new_filename = "{0}.{1}".format(now, test[1])
    f.save(secure_filename(new_filename))
    shutil.move(new_filename, "./Images/In")
    print('file uploaded successfully')
    return new_filename


def create_image_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)
        print("Directory {} Created ".format(path))
    else:
        print("Directory {} already exists".format(path))


def main(input_filename, output_filename, max_results):
    with open(input_filename, 'rb') as image:
        faces = detect_face(image, max_results)
        # Names of likelihood from google.cloud.vision.enums
        likelihood_name = ('UNKNOWN', 'VERY_UNLIKELY', 'UNLIKELY', 'POSSIBLE',
                           'LIKELY', 'VERY_LIKELY')
        print('Faces: from right to left')
        ret = []
        for face in faces:
            ret.append({'anger': likelihood_name[face.anger_likelihood],
                        'joy': likelihood_name[face.joy_likelihood],
                        'surprise': likelihood_name[face.surprise_likelihood],
                        'sorrow': likelihood_name[face.sorrow_likelihood]})

        print('Writing to file {}'.format(output_filename))
        # Reset the file pointer, so we can read the file again
        image.seek(0)
        highlight_faces(image, faces, output_filename)
        return ret


# if __name__ == "__main__":
#     main("bond.jpg", "test.jpg", 4) #Attention tu dois mettre le chemin vers le fichier que tu veux analyser et le nom du fichier que tu auras en retour
#     # implicit()

app = Flask(__name__)
CORS(app)


@app.route('/', methods=['POST', 'GET'])
def index():
    create_image_folder("./Images")
    create_image_folder("./Images/In")
    create_image_folder("./Images/Out")
    path = get_image()
    ret = json.dumps(main('./Images/In/{}'.format(path), "./Images/Out/ret_{}".format(path), 4))
    print(ret)
    return ret


if __name__ == "__main__":
    app.run(debug=True)  # , host='192.168.1.22')
